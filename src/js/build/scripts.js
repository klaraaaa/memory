var round = 0;
$( document ).ready(function() {
  renderBoard();
  $("#reset").click(function(){
    localStorage.clear();
    $("#board").empty()
    $(".done").empty();
    renderBoard();
  });
  $("#start").click(function(){
    $("#board").empty()
    $(".done").empty();
    renderBoard();
  });
});

function renderBoard(){
  round = localStorage.length + 1;
  var scores = [];
  var cards = [];
  var tries = 0;
  var pairs = 0;
  var totalCards = localStorage.length+2;
  var totalPairs = totalCards;
  for(var i = 0; i < totalCards; i++){
    cards.push("<div class='card unmatched' pic='"+i+"' value='" + i + "'> </div>", "<div class='card unmatched' pic='"+i+"' value='" + i + "'> </div>");
  }
  $("#board").append(cards.sort(function(){ return 0.5 - Math.random() }));
  $(".card").click(function(){
   if(!$(this).hasClass("flipped")){
    $(this).addClass( ($(".first-pick").length < 1) ? "first-pick flipped" : "second-pick flipped")
    var match = ($(".first-pick").attr("value") === $(".second-pick").attr("value") ) ? true : false;  
    $(".first-pick").addClass((match) ? "matched" : "" )
    $(".second-pick").addClass( (match) ? "matched" :  "")
    if($(".first-pick").length > 0 && $(".second-pick").length > 0){
      pairs = (match) ? pairs+1 : pairs;
      console.log(match,'match')
      tries++;
      $(".first-pick").addClass("flipped-fail")
      $(".first-pick").removeClass("first-pick flipped")
      $(".second-pick").removeClass("second-pick flipped")
      $(".second-pick").addClass("flipped-fail")
    }
    $(".done").html('<p class="feedback">'+tries+' tries</p><p class="feedback">'+pairs+'/'+totalPairs+' matches</p>');
    if (pairs === totalPairs) {
       $(".highscore").empty();
      localStorage.setItem(JSON.stringify(round), JSON.stringify(tries));
      for (var key in localStorage){
        $(".highscore").append('<p>Round '+key+': '+localStorage[key]+' tries</p>');
      }
    }
   }
  });
}